import Axios from 'axios';
import { IErply } from '../domain/IErply';
export abstract class ErplyApi {
    private static axios = Axios.create(
        {
            baseURL: "https://vat.erply.com/numbers?vatNumber=BG",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )
    static async getAll(vat: string): Promise<IErply> {
        debugger;
        const url = "https://vat.erply.com/numbers?vatNumber=" + vat;
        try {
            const response = await this.axios.get<IErply>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return {Address: '', CountryCode:'', Name: '', RequestDate:'', VATNumber:'', Valid: false};
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return {Address: '', CountryCode:'', Name: '', RequestDate:'', VATNumber:'', Valid: false};
        }
    }
}
