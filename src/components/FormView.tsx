import React from "react";
import { IFormState } from "./Home";
interface IProps{
    data: IFormState;
    handleSubmit: (event: React.MouseEvent<HTMLElement>) => void;
    handleChange: (target: EventTarget & HTMLInputElement) => void;

}
const FormView = (props: IProps) => (
    <form>
        <div className="form-group">
            <input value={props.data.input} onChange={(e) => props.handleChange(e.target)} name="input" type="text" className="form-control" id="vat" aria-describedby="vat" />
        </div> 
        <div className="text-center">     
        <button onClick={(e) => props.handleSubmit(e)} name="button" type="button" className="btn btn-primary" >Submit</button>
        </div> 

    </form>
);

export default FormView;