import { render } from "@testing-library/react";
import React, { useState } from "react";
import Erply from "./Erply";
import FormView from "./FormView";

export interface IFormState {
    input: string;
    showComponent: boolean;
}
const Home = () => {
    
    const [state, setState] = useState(
        {
            input: '',
            showComponent: false
        } as IFormState
    );
    const handleChange = (target: EventTarget & HTMLInputElement) => {
        setState({...state,...{input: target.value}, ...{showComponent: false} });
    }
    const handleSubmit = (event: React.MouseEvent<HTMLElement>) => {
        setState({...state, ...{showComponent: true}});
    }
    return (
        <>
            <h1 style={{textAlign: "center"}}>Chech VAT Number</h1>
            <FormView data={state} handleChange={handleChange} handleSubmit={handleSubmit}/>
            <br/>
            {state.showComponent ? <Erply vat={state.input} /> : null}
            
        </>
    );
}
export default Home;