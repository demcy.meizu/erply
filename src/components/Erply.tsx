import React from "react";
import { IErply } from "../domain/IErply";
import { ErplyApi } from "../services/ErplyApi";
interface IProps {
    vat: string
}
interface IState {
    vats: IErply;
}
export default class Erply extends React.Component<IProps, IState>{
    state: IState = {
        vats: { Address: '', CountryCode: '', Name: '', RequestDate: '', VATNumber: '', Valid: false }
    }
    props: IProps = {
        vat: ''
    }
    async componentDidMount() {
        const data = await ErplyApi.getAll(this.props.vat);
        debugger;
        this.setState({ vats: data });
    }
    render() {
        return (
            <>
                <h2 style={{ textAlign: "center" }}>VAT number {this.props.vat} is {this.state.vats.Valid === true ? 'Valid' : 'Not Valid'}</h2>
                <br />
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VATNumber</th>
                            <th>CountryCode</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>RequestDate</th>
                            <th>Valid</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{this.state.vats.VATNumber}</td>
                            <td>{this.state.vats.CountryCode}</td>
                            <td>{this.state.vats.Name}</td>
                            <td>{this.state.vats.Address}</td>
                            <td>{this.state.vats.RequestDate}</td>
                            <td>{this.state.vats.Valid === true ? 'Valid' : 'Not Valid'}</td>
                        </tr>
                    </tbody>
                </table>
            </>
        );
    }
}

