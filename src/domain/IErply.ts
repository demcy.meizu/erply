export interface IErply{
    CountryCode: string,
    VATNumber: string,
    RequestDate: string,
    Valid: boolean,
    Name: string,
    Address: string
}