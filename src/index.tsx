import 'jquery';
import 'popper.js';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import 'font-awesome/css/font-awesome.min.css';

import React from 'react';
import ReactDOM from 'react-dom';
import Home from './components/Home';


ReactDOM.render(
  <React.StrictMode>
    <>
        <div className="container">
            <main role="main" className="pb-3">
                <Home/>
            </main>
        </div>
    </>
  </React.StrictMode>,
  document.getElementById('root')
);